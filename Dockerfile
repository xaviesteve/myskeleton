FROM ubuntu:22.04

ENV PHP_VERSION=8.1
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/madrid

ARG user=www-data
ARG group=www-data


# Configure main packages, nginx, php and supervisor. 
RUN apt-get update && apt-get install -y --no-install-recommends \
    nginx \
    python3-certbot-nginx \
    git \
    zip \
    curl \
    wget \
    autoconf \   
    automake \
    make \
    gcc \
    g++ \
    bash \
    libicu-dev \
    libzip-dev \
    gettext-base \
    php${PHP_VERSION} \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-mysql \
    php${PHP_VERSION}-sockets \
    php${PHP_VERSION}-bcmath \
    php${PHP_VERSION}-intl \
    php${PHP_VERSION}-zip \
    php${PHP_VERSION}-opcache \
    php${PHP_VERSION}-xdebug \
    supervisor \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

COPY docker/rootfs /
RUN chmod +x /usr/local/bin/entrypoint.sh

ENV PHP_IDE_CONFIG 'serverName=DockerApp'
# Set working directory
WORKDIR /var/www

# Generate self-signed certificate
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
 -keyout /etc/ssl/private/nginx-selfsigned.key \
 -out /etc/ssl/certs/nginx-selfsigned.crt \
 -subj "/C=ES/ST=Barcelona/L=myskeleton/O=dev/CN=${HOSTNAME}"

# Generate self-signed dhparam
RUN openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
ENTRYPOINT [ "sh", "/usr/local/bin/entrypoint.sh" ]
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]